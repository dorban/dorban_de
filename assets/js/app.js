import "typeface-roboto"
import "typeface-source-code-pro"
import "../css/app"

// uikit
import "uikit/dist/css/uikit"

import UIkit from 'uikit/dist/js/uikit';
import Icons from 'uikit/dist/js/uikit-icons';

UIkit.use(Icons);

// jquery
import jQuery from 'jquery/dist/jquery';
window.$ = window.jQuery = jQuery;