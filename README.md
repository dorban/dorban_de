# Page from dorban.de

## CMS

I use the Kirby CMS. https://getkirby.com/

## Layout

It is a really simple layout with scss. It doesn't use any Javascript at the moment.

### Watch and build

You need _inotfy-tools_ and _ruby_sass_ to build the layout.

Under a debian based Distribution:

```
apt install ruby-sass inotify-tools
```

Then simply run:

```
./assets.sh build # or
./assets.sh watch
```

## Content

I will add dummy content later. My content is archived in a private git repository.