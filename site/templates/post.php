<?php snippet('header') ?>
<?php snippet('offCanvas') ?>

<main class="content">
    <article class="uk-article">
        <h1 class="uk-article-title"><?= $page->title() ?></h1>

        <div class="uk-article-meta">
            <time datetime="<?= $page->date() ?>">
                <?= $page->date()->toDate('d.m.Y') ?>
            </time>
        </div>

        <?php if ($page->intro()->isNotEmpty()) : ?>       
            <div class="uk-text-lead">
                <?= $page->intro()->kirbytext() ?>
            </div>
        <?php endif ?>
    
        <?= $page->text()->kirbytext() ?>

        <div class="uk-child-width-1-3@m" uk-grid uk-lightbox="animation: scale">
            <?php foreach ($page->gallery()->toFiles() as $item) : ?>
                <a href="<?= $item->resize(1920, 1920)->url() ?>"
                   title="<?= $item->caption() ?>" data-caption="<?= $item->caption() ?>">
                    <?= $item->crop(400, 300) ?>
                </a>
            <?php endforeach ?>
        </div>
    </article>

    <a href="<?= $page->parent()->url() ?>">
        <span uk-icon="chevron-double-left"></span> Zurück
    </a>
</main>

<?php snippet('footer') ?>