<?php snippet('header') ?>
<?php snippet('offCanvas') ?>

<main class="content">
    <h1><?= $page->title() ?></h1>
    <?= $page->text()->kirbytext() ?>
</main>

<?php snippet('footer') ?>