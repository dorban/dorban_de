<?php snippet('header') ?>
<?php snippet('offCanvas') ?>

<main class="content">
    <h1><?= $page->title() ?></h1>
    
    <div class="templates-posts">
        <?php foreach ($posts as $post) : ?>
            <article class="templates-posts_article">
                <div class="templates-posts_date">
                    <time datetime="<?= $post->date() ?>">
                        <?= $post->date()->toDate('d.m.Y') ?>
                    </time>
                </div>
                <div class="templates-posts_intro">
                    <a href="<?= $post->url() ?>">
                        <h3 class="templates-posts_title"><?= $post->title() ?></h3>
                    </a>

                    <?php if ($post->preview()->toFile()) : ?>
                        <a href="<?= $post->url() ?>" aria-label="Beitragsbild - <?= $post->title() ?>">
                            <?= $post->preview()->toFile()->resize(400, 400) ?>
                        </a>
                    <?php endif ?>

                    <?php if ($post->intro()->isNotEmpty()) : ?>
                        <?= $post->intro()->kirbytext() ?>
                    <?php else : ?>
                        <?= $post->text()->excerpt(100)->kirbytext() ?>
                    <?php endif ?>

                    <a href="<?= $post->url() ?>">
                        <span uk-icon="chevron-double-right"></span> Mehr
                    </a>
                </div>
            </article>
        <?php endforeach ?>
    </div>
</main>

<?php snippet('footer') ?>