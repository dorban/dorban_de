<meta property="og:site_name" content="<?= $site->title() ?>" />
<meta property="og:type" content="website" />
<meta name="twitter:card" content="summary_large_image" />

<?php if ($page->seoTitle()->isNotEmpty()) : ?>
    <meta property="og:title" content="<?= $page->seoTitle() ?>" />
    <meta name="twitter:title" content="<?= $page->seoTitle() ?>" />
<?php else : ?>
    <meta property="og:title" content="<?= $page->title() ?>" />
    <meta name="twitter:title" content="<?= $page->title() ?>" />
<?php endif ?>

<?php if ($page->seoDescription()->isNotEmpty()) : ?>
    <meta name="description" content="<?= $page->seoDescription() ?>" />
    <meta property="og:description" content="<?= $page->seoDescription() ?>" />
    <meta name="twitter:description" content="<?= $page->seoDescription() ?>" />
<?php elseif ($site->seoDescription()->isNotEmpty()) : ?>
    <meta name="description" content="<?= $site->seoDescription() ?>" />
    <meta property="og:description" content="<?= $site->seoDescription() ?>" />
    <meta name="twitter:description" content="<?= $site->seoDescription() ?>" />
<?php endif ?>

<?php if ($site->seoUrl()->isNotEmpty()) : ?>
    <meta property="og:url" content="<?= $site->seoUrl() . '/' . $page->uri() ?>" />
    <meta name="twitter:url" content="<?= $site->seoUrl() . '/' . $page->uri() ?>" />
<?php else : ?>
    <meta property="og:url" content="<?= $page->url() ?>" />
    <meta name="twitter:url" content="<?= $page->url() ?>" />
<?php endif ?>

<?php
if ($page->seoImage()->isNotEmpty() && $page->seoImage()->toFile())
    $seoImage = $page->seoImage()->toFile();
elseif ($site->seoImage()->isNotEmpty() && $site->seoImage()->toFile())
    $seoImage = $site->seoImage()->toFile();
else
    $seoImage = false;
?>

<?php if ($seoImage) : ?>
    <meta property="og:image" content="<?= $seoImage->url() ?>" />
    <meta property="og:image:width" content="<?= $seoImage->width() ?>" />
    <meta property="og:image:height" content="<?= $seoImage->height() ?>" />
    <meta name="twitter:image" content="<?= $seoImage->url() ?>" />
<?php endif ?>

<?php if ($page->isHomePage()) : ?>
    <link rel="canonical" href="<?= $site->seoUrl() ?>" />
<?php else : ?>
    <link rel="canonical" href="<?= $site->seoUrl() . '/' . $page->uri() ?>" />
<?php endif ?>

<?php if ($site->seoIndex() == 'noindex' || $page->seoIndex() == 'noindex') : ?>
    <meta name="robots" content="noindex, nofollow" />
<?php endif ?>