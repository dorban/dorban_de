<div class="snippet-offCanvas">
    <div id="navigation" data-uk-offcanvas="mode: reveal">
        <div class="uk-offcanvas-bar">
            <a class="snippet-offCanvas_title" href="<?= $site->url() ?>">
                <span data-uk-icon="icon: paint-bucket; ratio: 4"></span> <?= $site->title() ?>
            </a>
            <ul class="uk-nav uk-nav-default">
                <?php foreach ($site->children()->listed() as $item) : ?>
                    <li>
                        <a href="<?= $item->url() ?>">
                            <span class="uk-margin-small-right" 
                                  data-uk-icon="icon: <?php e($item->uiKitIcon()->isNotEmpty(), $item->uiKitIcon(), 'chevron-right') ?>"></span>
                            <?= $item->title() ?>
                        </a>
                    </li>
                <?php endforeach ?>
            </ul>
        </div>
    </div>

    <a class="snippet-offCanvas_kicker" href="#navigation" data-uk-toggle aria-label="Navigation öffnen oder schließen" >
        <span data-uk-icon="icon: menu; ratio: 2"></span>
    </a>
</div>
