<?php

return function ($page, $request) {

    if (param('c')) {
        $posts = $page->children()->filterBy('category', param('c'))->listed()->sortBy('date', 'DESC');
    } else {
        $posts = $page->children()->listed()->sortBy('date', 'DESC');
    }

    return [
        "posts" => $posts
    ];
};